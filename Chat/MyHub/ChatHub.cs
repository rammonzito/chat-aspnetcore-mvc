
using System.Threading.Tasks;
using Chat.Models;
using Chat.Repositories;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace Chat.MyHub
{
    public class ChatHub : Hub
    {
        private readonly static ConnectionRepo _connections = new ConnectionRepo();

        public override Task OnConnectedAsync()
        {
            var user = JsonConvert.DeserializeObject<User>(Context.GetHttpContext().Request.Query["user"]);
            _connections.Add(Context.ConnectionId, user);

            Clients.All.SendAsync("chat", _connections.GetAllUsers(), user);

            return base.OnConnectedAsync();

        }
 
        /// Método responsável por encaminhar as mensagens pelo hub
        public async Task SendMessage(Message chat)
        {
            await Clients.Client(_connections.GetUserID(chat.destination)).SendAsync("Receive", chat.sender, chat.message);
        }
    }
}